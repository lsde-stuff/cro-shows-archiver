# cro-shows-archiver

## About
This script uses mujRozhlas API to scrape and download all episodes of a defined show.

## Requirements
    python >= 3
    python-requests
    python-slugify
    youtube-dl

## Usage
    $ virtualenv venv
    ..
    $ source venv/bin/activate
    $ pip install -r requirements.txt
    ..
    ./cro-shows-archiver.py -h
    usage: cro-shows-archiver.py [-h] [-d DIRECTORY] [-s SHOW]
    
    options:
      -h, --help            show this help message and exit
      -d DIRECTORY, --directory DIRECTORY
                            Target directory
      -s SHOW, --show SHOW  Show to download

## Deploy of k8s CronJob to scrape every 12h
    $ kubectl create namespace cro-shows-archiver
    $ kubectl -n cro-shows-archiver apply -f cro-shows-archiver.yaml


