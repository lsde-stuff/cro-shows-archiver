FROM python:alpine
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
RUN apk add -U ffmpeg
COPY cro-shows-archiver.py ./
VOLUME /data
ENV DIRECTORY /data
USER nobody
CMD [ "python", "./cro-shows-archiver.py" ]
