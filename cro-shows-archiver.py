#!/usr/bin/env python
import requests
from slugify import slugify
import youtube_dl
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--directory', help='Target directory', default=os.getenv('DIRECTORY', '.'))
parser.add_argument('-s', '--show', help='Show to download', default=os.getenv('SHOW', '18ce61f4-31fd-3240-ae4c-b14732658722'))
args = parser.parse_args()

r = requests.get(f'https://api.mujrozhlas.cz/shows/{args.show}/episodes')
episodes = r.json()['data']

for episode in episodes:
    sanitized_show = slugify(episode['attributes']['mirroredShow']['title'], allow_unicode=False)
    if 'mirroredSerial' in episode['attributes'].keys():
        sanitized_title = slugify(episode['attributes']['mirroredSerial']['title'], allow_unicode=False)
        part = episode['attributes']['part']
        total_parts = episode['attributes']['mirroredSerial']['totalParts']
        filename = f"{args.directory}/{sanitized_show}_{sanitized_title}_{part}of{total_parts}_{episode['id']}"
    else:
        sanitized_title = slugify(episode['attributes']['title'], allow_unicode=False)
        filename = f"{args.directory}/{sanitized_show}_{sanitized_title}_{episode['id']}"
    ydl_opts = {'outtmpl': f'{filename}.%(ext)s', "cachedir": False}
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([episode['attributes']['audioLinks'][0]['url']])
